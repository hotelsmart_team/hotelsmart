export const state = () => ({
  searchIndex: {}
})

export const mutations = () => ({

  saveSearchIndex(state, data){
    state.searchIndex = data
  }
  
})

export const actions = () => ({

  saveSearchIndex({commit}, data){
    commit('saveSearchIndex', data)
  }

})