// import Vue from 'vue'
// import Vuex from 'vuex'
import { firebase, db } from '@/plugins/firebase'

export const strict = false
export const state = () => ({
  auth: {},
  userInfo: null,
  notifications: [],
  listOfHotels: [],
  cart: [],
  smart_hotel: {},
  myCheckins: [],
  nairaSign: '&#x20A6;'
})

export const mutations = {
  setUser(state,data){
    state.auth = Object.assign({}, {
      loggedIn: true,
      user: ['displayName', 'photoURL','email','phone','uid']
      .reduce((a, e) => (a[e] = data[e], a), {}),
    })
  },
  setUserInfo(state, data){
    state.userInfo = data
  },
  logout(state, data){
    
    firebase.auth().signOut().then(()=>{
      state.auth = Object.assign({}, {
        loggedIn: false,
        user: null
      })

    })
  },
  addToCart(state, data){
    state.cart.push(data)
  },
  removeFromCart(state, item){
    let itemIndex = state.cart.indexOf(item)
    state.cart.splice(itemIndex, 1)
  },
  clearCart(state){
    try {
      
      state.cart = state.cart.filter(item => item.hotelId != state.smart_hotel.docId)
      // console.log(state.cart)

    } catch (error) {
      console.log(error)
    }
  },
  setNotifications(state, data){
    state.notifications = data
  },
  saveHotelsList(state, data){
    state.listOfHotels = data
  },
  setSmartHotel(state, data){
    state.smart_hotel = data
  },
  myCheckins(state, data){
    state.myCheckins = data
  },
}

export const actions = {
  setUser({commit},data){
    commit('setUser', data)
  },
  setUserInfo({commit}, data){
    commit('setUserInfo', data)
  },
  logout({commit},data){
    commit('logout', data)
  },
  setNotifications({commit},data){
    commit('setNotifications', data)
  },
  saveHotelsList({commit}, data){
    commit('saveHotelsList', data)
  },
  addToCart({commit}, data){
    commit('addToCart', data)
  },
  removeFromCart({commit}, data){
    commit('removeFromCart', data)
  },
  clearCart({commit}, data){
    commit('clearCart', data)
  },
  setSmartHotel({commit}, data){
    commit('setSmartHotel', data)
  },
  myCheckins({commit}, data){
    commit('myCheckins', data)
  },
}

export const getters = {
  unread_notifications: state => {
    // if(state.userInfo && state.userInfo.reviews){

    //   return state.notifications.filter(notif => 
    //   !state.userInfo.reviews.includes(notif.from.hotelId)
    //   )
    // }
    // else {return state.notifications}

    return state.notifications.filter(item =>

      !item.read
    )
  },
  getCart: state => {
    return state.cart.filter(item => item.hotelId == state.smart_hotel.docId)
  }
}