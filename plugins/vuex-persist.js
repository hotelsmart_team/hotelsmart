import VuexPersistence from 'vuex-persist'
 
export default ({store}) => {
  return new VuexPersistence({
    reducer: (state) => ({
      auth: state.auth
    }),
  }).plugin(store);
}