import Vue from 'vue'
import VuePaystack from 'vue-paystack'

if(!process.server){

  Vue.component('paystack', VuePaystack)
}