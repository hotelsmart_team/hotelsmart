import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';

let config = {
  apiKey: "AIzaSyBuQnAPb7bCOFYYB2zmreWK1w2giINl0Ps",
  authDomain: "hotelsmart-a28dd.firebaseapp.com",
  databaseURL: "https://hotelsmart-a28dd.firebaseio.com",
  projectId: "hotelsmart-a28dd",
};

// global.firebase = firebase
// global.app = firebase.initializeApp(config);

// console.log({is_server: process.server, is_static: process.static})

let db = ''
let database = ''
let app = !firebase.apps.length ? firebase.initializeApp(config) : ''

if(!process.server){

  database = firebase.database();
  db = firebase.firestore();
  db.enablePersistence({synchronizeTabs: true})
  .catch(function(err) {
    if (err.code == 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
        console.log(err)
    } else if (err.code == 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      // ...
      console.log(err)
    }
  });
  
  
}
export { firebase, db, database, app }

