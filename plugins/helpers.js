import Vue from 'vue'
import helpers from '@/helpers/helpers'
import firebase from '@/plugins/firebase'

Vue.prototype.$eventBus = new Vue();

const plugin = {
  install () {
      Vue.helpers = helpers
      Vue.prototype.$helpers = helpers
  }
}

Vue.use(plugin)