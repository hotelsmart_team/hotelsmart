'use strict'
export default {
  truncateText(text, length=18){
    return typeof text === 'string' ?
      text.length > length ?
      text.substr(0, length) + '...' :
      text :
      text
  },
  trigFileSelector(){
    document.getElementById('file_img').click()
  },
  parseDate(timestamp, show_date=true){
    // show_date is used to control which type of date to return - full date or only time
    // USING CLIENT TIME. MAYBE USE SERVER TIME ?
    let d = new Date(timestamp)
    let now = Date.now()
    let diff = now - timestamp

    let one_sec = 1000
    let one_min = one_sec * 60
    let one_hour = one_min * 60
    let one_day = one_hour * 24

    let options = {hour:'numeric', minute:'numeric' };
    let options2 = {
      weekday: 'short', 
      year: 'numeric', 
      month: 'short', 
      hour:'numeric', 
      minute:'numeric'
    }
  
    if(diff < 1000){
      return 'just now'
    }
    if(diff > one_sec && diff < one_min){
      return 'a few seconds ago'
    }
    if(diff > one_min && diff < one_hour){
      return Math.floor(diff / one_min) + ' minutes ago'
    }
    if(diff >= one_hour && diff < 2 * one_hour){
      return 'an hour ago'
    }
    if(diff > one_hour && diff < one_day){
      return Math.floor(diff/one_hour)  + ' hours ago'
    }
    if(diff > one_day && diff < one_day * 2){
      return 'yesterday, ' + d.toLocaleString("en-US",options)
    }
    if(diff >= one_day * 2 && diff < one_day * 3){
      return '2d ago at ' + d.toLocaleString("en-US", show_date ? options2 : options)
    }
    return d.toLocaleString("en-US", show_date ? options2 : options)
  },
  // async uploadImage(files,preset){
  //   // console.log(files, preset)
  //   try {
  //     let clUrl = `https://api.cloudinary.com/v1_1/${preset.cloud_name}/upload`
  //     let formData = new FormData()
  //     let uploaded = []
  //     for(let file of files){
  //       formData.append('file', file)
  //       formData.append('upload_preset', preset.upload_preset)

  //       let response = await axios.post( clUrl,
  //         formData,
  //         {
  //           headers: {
  //               'Content-Type': 'multipart/form-data'
  //           }
  //         }
  //       )

  //       uploaded.push(response.data.secure_url)
  //     }
  //     // eslint-disable-next-line
  //     console.log(uploaded)
  //     return uploaded
  //   } catch (error) {
  //     // eslint-disable-next-line
  //     console.log(error)
  //     alert('Image upload failed')
  //     throw new Error(error || error.response)
      
  //   }
  // }
}

// import axios from 'axios'