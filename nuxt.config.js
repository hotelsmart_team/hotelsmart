const pkg = require('./package')
import { db } from './plugins/firebase'

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: 'The smart way to find discounted hotels online - HotelSmart',
    meta: [
      { charset: 'utf-8' },
      { hid: 'viewport', name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Find and book hotels in Nigeria at the cheapest rate. Compare rates, see accurate reviews and real pictures and videos. ✔Convenient payment, ✔Online Checkin, ✔Book now, paylater' }
    ],
    script: [
      { src: 'https://js.api.here.com/v3/3.0/mapsjs-core.js' },
      { src: 'https://js.api.here.com/v3/3.0/mapsjs-service.js' },
      { src: 'https://js.api.here.com/v3/3.0/mapsjs-mapevents.js' },
      { src: 'https://js.api.here.com/v3/3.0/mapsjs-ui.js' },
      // { src: 'https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.js' }
    ],
    link: [
      { rel: 'stylesheet', href: "https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1549984893" },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      // {
      //   rel: 'stylesheet',
      //   href: "https://s3.amazonaws.com/assets.freshdesk.com/widget/freshwidget.css"
      // }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '~/assets/style/app.styl'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    '@/plugins/helpers',
    '@/plugins/vuelidate',
    '@/plugins/vue-paystack.client.js',
    '@/plugins/vue-gallery.client.js',
    { src: '@/plugins/ga.js', mode: 'client' },
    { src: '@/plugins/firebaseui', mode: 'client' }
  ],

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:5050',
    hereAppId: '91mCqHok3ztSIWE6ScWL',
    hereAppCode: 'h-I2PFaLa-2IswMnXuZ5ew'
  },
  server: {
    port: 3000, // default: 3000
    // host: '0.0.0.0' // default: localhost
  },
  router: {
    // middleware: 'auth'
  },

  
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    // '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // '@nuxtjs/auth',
    '@nuxtjs/dotenv',
    '@nuxtjs/sitemap',
    '@nuxtjs/google-analytics'
  ],
  googleAnalytics: {
    id: 'UA-151472904-1'
  },
  /*
  ** Axios module configuration
  */
  // axios: {
  //   // See https://github.com/nuxt-community/axios-module#options
  //   baseURL:'http://localhost:5000',
  // },

  /*proxy: {
    '/api/': 'https://hotelsmartapi.herokuapp.com',
    // '/api/': 'http://localhost:5000',
    '/api2/': 'http://api.another-website.com'
  }, */
  // auth: {
  //   strategies: {
  //     local: {
  //       endpoints: {
  //         login: { url: '/api/auth/login', method: 'post', propertyName: 'token' },
  //         logout: false,
  //         user: { url: '/api/auth/user', method: 'get', propertyName: 'user' }
  //       },
  //       tokenRequired: true,
  //       tokenType: 'bearer'
  //     }
  //   }
  // },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ["~assets/style/variables.styl"]
      }
    },
    
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  },
  pwa: {
    // manifest: {
    //   name: 'Hotelsmart'
    // }
  },

  sitemap: {
    hostname: 'https://hotelsmart.com.ng',
    gzip: true,
    exclude: [
      '/account',
      '/login',
      '/signup'
    ],
    
  },

  generate: {
    fallback: true,
    subFolders: true,
    routes: async function(){

      let docs = await db.collection('hotels')
      .where('active_status', '==', 'active')
      .get()
      
      let hotels = []
      
      docs.forEach(hotel => {
        hotels.push({
          route: `/hotels/${hotel.data().docId}`,
          payload: hotel.data()
        })
      })

      let destinations = [
        '/destinations/abuja',
        '/destinations/lagos',
        '/destinations/kano',
        '/destinations/enugu',
        '/destinations/port harcourt',
        '/destinations/benin',
      ]
      

      // return Promise.all([destinations, hotels]).then(values => {
      //   return values
      // })

      return destinations.concat(hotels)
      
    }
  }
}
